package com.mdf3.justin.shifty.Fragment.Announcements;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.AnnouncementAdapter;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.AnnouncementItem;
import com.mdf3.justin.shifty.R;

import java.util.ArrayList;

/**
 * Created by Torres on 10/29/17.
 */

public class AnnouncementFragmentPhone extends Fragment implements AdapterView.OnItemClickListener{

    ListView announcementList;
    ArrayList<AnnouncementItem> mList;
    private ArrayList<AnnouncementItem> deleteArray;
    AnnouncementAdapter mAdapter;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private Boolean mEdit = false;
    Menu mMenu;


    public static AnnouncementFragmentPhone newInstance() {
        Bundle args = new Bundle();
        AnnouncementFragmentPhone fragment = new AnnouncementFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        menu.findItem(R.id.announcement_add_menu).setVisible(true);
        menu.findItem(R.id.announcement_edit_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.announcement_add_menu:
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        AddAnnouncementFragment.newInstance()
                ).addToBackStack(null).commit();
                break;
            case R.id.announcement_edit_menu:
                editAnnouncements();
                break;
            case R.id.save_announcement_menu:
                deleteAnnouncements();
                break;
            case R.id.cancel_announcement_menu:
                cancelAnnouncements();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.announcement_fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        announcementList = (ListView) getActivity().findViewById(R.id.announcement_list);
        mList = new ArrayList<>();
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        mAdapter = new AnnouncementAdapter(getActivity(), mList, mEdit);
        announcementList.setAdapter(mAdapter);
        announcementList.setOnItemClickListener(this);
        pullAnnouncements();


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(mEdit){
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.announcement_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);
                //mList.add(deleteArray.get(i));
                deleteArray.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteArray.add(mList.get(i));
                //mList.remove(deleteArray.get(i));
            }
        } else {
            Bundle detailBundle = new Bundle();
            AnnouncementDetailFragmentPhone fragment = AnnouncementDetailFragmentPhone.newInstance();
            detailBundle.putString("announcementCompanyStringDetail", mCompanyId);
            detailBundle.putString("announcementID", mList.get(i).getUuid());
            fragment.setArguments(detailBundle);
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    fragment
            ).addToBackStack(null).commit();
        }
    }

    public void editAnnouncements(){
        deleteArray = new ArrayList<>();
        mEdit = true;
        mAdapter.updateEdit(true);
        mMenu.findItem(R.id.announcement_add_menu).setVisible(false);
        mMenu.findItem(R.id.announcement_edit_menu).setVisible(false);
        mMenu.findItem(R.id.save_announcement_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_announcement_menu).setVisible(true);
    }
    public void deleteAnnouncements(){
        for(AnnouncementItem announcement: deleteArray){
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements")
                    .child(announcement.getUuid());
            ref.removeValue();
        }
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                AnnouncementFragmentPhone.newInstance()
        ).commit();
    }
    public void cancelAnnouncements(){
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.announcement_add_menu).setVisible(true);
        mMenu.findItem(R.id.announcement_edit_menu).setVisible(true);
        mMenu.findItem(R.id.save_announcement_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_announcement_menu).setVisible(false);
    }


    //Firebase
    public void pullAnnouncements(){
        DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mList.add(new AnnouncementItem(dataSnapshot.child("title").getValue(String.class), dataSnapshot.child("date").getValue(String.class), dataSnapshot.getKey()));
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mAdapter.notifyDataSetChanged();
            }
        });
    }
}
