package com.mdf3.justin.shifty.Fragment.Schedule;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mdf3.justin.shifty.Helper.ScheduleHelper.DailyScheduleItem;
import com.mdf3.justin.shifty.Helper.ScheduleHelper.DailyScheduleListViewAdapter;
import com.mdf3.justin.shifty.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Torres on 10/26/17.
 */

public class ScheduleDetailFragmentPhone extends Fragment {

    private static final String TAG = "ScheduleDetailFragmentP";
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private ArrayList<DailyScheduleItem> mList;
    private DailyScheduleListViewAdapter adapter;
    private ListView list_view;
    private ArrayList<String> week_days;
    String mCompanyId;

    public static ScheduleDetailFragmentPhone newInstance(String selectedDay) {

        Bundle args = new Bundle();
        args.putString("selected_day", selectedDay);
        ScheduleDetailFragmentPhone fragment = new ScheduleDetailFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_detail_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        list_view = (ListView) getActivity().findViewById(R.id.schedule_detail_list);
        mList = new ArrayList<>();
        adapter = new DailyScheduleListViewAdapter(getContext(), mList);
        list_view.setAdapter(adapter);
        pullFromFirebase(getArguments().getString("selected_day"));
        super.onActivityCreated(savedInstanceState);
    }

    public void pullFromFirebase(String selectedDay){
        Log.d(TAG, "pullFromFirebase: " + selectedDay);
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("schedule").child(selectedDay);
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot != null) {
                    mList.add(new DailyScheduleItem(dataSnapshot.child("image").getValue(String.class), dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("shift-start").getValue(String.class) + "-" + dataSnapshot.child("shift-end").getValue(String.class)));
                }
                Collections.sort(mList, new Comparator<DailyScheduleItem>(){
                    @Override
                    public int compare(DailyScheduleItem r1, DailyScheduleItem r2) {
                        return r1.getTime().substring(0, 5).compareTo(r2.getTime().substring(0,5));
                    }
                });
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                adapter.notifyDataSetChanged();
            }
        });
    }


}
