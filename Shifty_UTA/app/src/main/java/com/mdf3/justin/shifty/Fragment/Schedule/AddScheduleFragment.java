package com.mdf3.justin.shifty.Fragment.Schedule;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mdf3.justin.shifty.Helper.EmployeeHelper.EmployeeAdapterTablet;
import com.mdf3.justin.shifty.Helper.EmployeeHelper.EmployeeItem;
import com.mdf3.justin.shifty.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Torres on 10/25/17.
 */

public class AddScheduleFragment extends Fragment implements AdapterView.OnItemClickListener, CalendarView.OnDateChangeListener{
    private static final String TAG = "AddScheduleFragment";
    private ListView employeeList;
    private ArrayList<EmployeeItem> mList;
    String mCompanyId;
    String selectedDate;
    String selectedShiftStart;
    String selectedShiftEnd;
    String selectedImage;
    Calendar mCalendar = Calendar.getInstance();
    private EmployeeAdapterTablet mAdapter;
    String selectedEmployee;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    CalendarView mainCalendar;
    TimePicker shiftStart;
    TimePicker shiftEnd;
    UUID randomUUID;

    public static AddScheduleFragment newInstance() {

        Bundle args = new Bundle();

        AddScheduleFragment fragment = new AddScheduleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.save_schedule_menu).setVisible(true);
        menu.findItem(R.id.cancel_schedule_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.save_schedule_menu){
            saveToSchedule();
        } else if (item.getItemId() == R.id.cancel_schedule_menu){
            cancelAddSchedule();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_schedule_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        employeeList = (ListView) getActivity().findViewById(R.id.add_schedule_employee_list);
        mainCalendar = (CalendarView) getActivity().findViewById(R.id.add_schedule_calendar);
        shiftStart = (TimePicker) getActivity().findViewById(R.id.shift_start);
        shiftEnd = (TimePicker) getActivity().findViewById(R.id.shift_end);
        employeeList.setSelector(R.color.colorPrimary);
        mList = new ArrayList<>();
        mAdapter = new EmployeeAdapterTablet(getContext(), mList, false);
        employeeList.setAdapter(mAdapter);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        employeeList.setOnItemClickListener(this);
        mainCalendar.setOnDateChangeListener(this);
        shiftStart.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                selectedShiftStart = String.format(Locale.US, "%02d:%02d", timePicker.getHour(), timePicker.getMinute());

            }
        });
        shiftEnd.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                selectedShiftEnd = String.format(Locale.US, "%02d:%02d", timePicker.getHour(), timePicker.getMinute());
            }
        });
        selectedDate = dateFormat.format(mCalendar.getTime());
        pullEmployeesForScheduleFromFirebase();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedEmployee = mList.get(i).getName();
        selectedImage = mList.get(i).getImage();
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
        mCalendar.set(i, i1, i2);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        selectedDate = dateFormat.format(mCalendar.getTime());
    }

    public void saveToSchedule(){
        if(selectedEmployee != null){

            String uuid = UUID.randomUUID().toString();
            Log.d(TAG, "saveToSchedule: " + selectedShiftStart);
                DatabaseReference ref = database.getReference().child("Companies")
                        .child(mCompanyId).child("schedule")
                        .child(selectedDate)
                        .child(uuid);
                ref.child("shift-start").setValue(selectedShiftStart);
                ref.child("shift-end").setValue(selectedShiftEnd);
                ref.child("name").setValue(selectedEmployee);
                ref.child("image").setValue(selectedImage);
                ref.child("schedule-key").setValue(uuid);
        } else {
            Toast.makeText(getContext(), "Please Select Employee", Toast.LENGTH_SHORT).show();
        }
    }
    public void cancelAddSchedule(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ScheduleFragment.newInstance()
        ).commit();
    }
    //Firebase
    public void pullEmployeesForScheduleFromFirebase(){
        //company id was already found
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mList.add(new EmployeeItem(dataSnapshot.child("name").getValue(String.class),
                        dataSnapshot.child("image").getValue(String.class), dataSnapshot.getKey(), dataSnapshot.child("occupation").getValue(String.class)));
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mAdapter.notifyDataSetChanged();
            }
        });
    }
}
