package com.mdf3.justin.shifty.Helper.AnnouncementHelper;

/**
 * Created by Torres on 10/29/17.
 */

public class AnnouncementItem {
    String title;
    String datePosted;
    String uuid;

    public AnnouncementItem(String title, String datePosted, String uuid) {
        this.title = title;
        this.datePosted = datePosted;
        this.uuid = uuid;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public String getTitle() {
        return title;
    }

    public String getUuid() {
        return uuid;
    }
}
