package com.mdf3.justin.shifty.Fragment.Announcements;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.AnnouncementAdapter;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.AnnouncementItem;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.CommentHelper.CommentAdapter;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.CommentHelper.CommentItem;
import com.mdf3.justin.shifty.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Torres on 10/30/17.
 */

public class AnnouncementFragmentTablet extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {
    ListView announcementList;
    ArrayList<AnnouncementItem> mList;
    ArrayList<AnnouncementItem> deleteArray;
    AnnouncementAdapter mAdapter;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    ConstraintLayout detailCs;
    TextView title;
    String tempSelected;
    TextView author;
    TextView date;
    TextView body;
    ListView commentList;
    Button addComment;
    ArrayList<CommentItem> mCommentList;
    CommentAdapter mCommentAdapter;
    private Boolean mEdit = false;
    Menu mMenu;

    public static AnnouncementFragmentTablet newInstance() {
        Bundle args = new Bundle();
        AnnouncementFragmentTablet fragment = new AnnouncementFragmentTablet();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        menu.findItem(R.id.save_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_edit_schedule_menu).setVisible(false);
        menu.findItem(R.id.save_announcement_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_announcement_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_new_employee_menu).setVisible(false);
        menu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        menu.findItem(R.id.announcement_add_menu).setVisible(true);
        menu.findItem(R.id.announcement_edit_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.announcement_add_menu:
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        AddAnnouncementFragment.newInstance()
                ).addToBackStack(null).commit();
                break;
            case R.id.announcement_edit_menu:
                editAnnouncements();
                break;
            case R.id.save_announcement_menu:
                deleteAnnouncements();
                break;
            case R.id.cancel_announcement_menu:
                cancelAnnouncements();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.announcement_fragment_tablet, container, false);
    }

    public void editAnnouncements(){
        deleteArray = new ArrayList<>();
        mEdit = true;
        mAdapter.updateEdit(true);
        mMenu.findItem(R.id.announcement_add_menu).setVisible(false);
        mMenu.findItem(R.id.announcement_edit_menu).setVisible(false);
        mMenu.findItem(R.id.save_announcement_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_announcement_menu).setVisible(true);
    }

    public void deleteAnnouncements(){
        for(AnnouncementItem announcement: deleteArray){
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements")
                    .child(announcement.getUuid());
            ref.removeValue();
        }
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                AnnouncementFragmentTablet.newInstance()
        ).commit();
    }

    public void cancelAnnouncements(){
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.announcement_add_menu).setVisible(true);
        mMenu.findItem(R.id.announcement_edit_menu).setVisible(true);
        mMenu.findItem(R.id.save_announcement_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_announcement_menu).setVisible(false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        announcementList = (ListView) getActivity().findViewById(R.id.announcement_list_tablet);
        mList = new ArrayList<>();
        mCommentList = new ArrayList<>();
        mAdapter = new AnnouncementAdapter(getActivity(), mList, mEdit);
        announcementList.setAdapter(mAdapter);
        detailCs = (ConstraintLayout) getActivity().findViewById(R.id.announcement_detail_tablet_container);
        detailCs.setVisibility(View.INVISIBLE);
        title = (TextView) getActivity().findViewById(R.id.announcement_detail_tablet_title);
        author = (TextView) getActivity().findViewById(R.id.announcement_detail_tablet_author);
        date = (TextView) getActivity().findViewById(R.id.announcement_detail_tablet_date);
        body = (TextView) getActivity().findViewById(R.id.announcement_detail_tablet_body);
        addComment = (Button) getActivity().findViewById(R.id.announcement_detail_add_comment);
        commentList = (ListView) getActivity().findViewById(R.id.announcement_detail_tablet_comment_list);
        announcementList.setOnItemClickListener(this);
        addComment.setOnClickListener(this);
        pullAnnouncements();
    }

    @Override
    public void onClick(View view) {
        if(addComment.isEnabled()){
            AlertDialog.Builder newPasswordDialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater customInflator = getActivity().getLayoutInflater();
            final View dialogView = customInflator.inflate(R.layout.edit_profile_change_password_dialog, null);
            newPasswordDialogBuilder.setView(dialogView);
            final EditText newPasswordET = (EditText) dialogView.findViewById(R.id.new_password_input);
            newPasswordET.setInputType(InputType.TYPE_CLASS_TEXT);
            newPasswordDialogBuilder.setTitle("Add Comment");
            newPasswordDialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(newPasswordET.getText().toString().isEmpty()){
                        Toast.makeText(getContext(), "Comment was empty", Toast.LENGTH_SHORT).show();
                    } else {
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Companies").child(mCompanyId)
                                .child("announcements").child(tempSelected).child("comments").child(UUID.randomUUID().toString());
                        ref.child("comment").setValue(newPasswordET.getText().toString());
                        ref.child("authorID").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
                        String date = dateFormat.format(cal.getTime());
                        ref.child("date-posted").setValue(date);
                        Toast.makeText(getContext(), "Comment Posted", Toast.LENGTH_SHORT).show();
                        mAdapter.notifyDataSetChanged();
                    }
                }
            });
            newPasswordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            AlertDialog mainAlert = newPasswordDialogBuilder.create();
            mainAlert.show();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(mEdit){
            detailCs.setVisibility(View.INVISIBLE);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.announcement_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);
                //mList.add(deleteArray.get(i));
                deleteArray.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteArray.add(mList.get(i));
                //mList.remove(deleteArray.get(i));
            }
        } else {
            mCommentList = new ArrayList<>();
            mCommentAdapter = new CommentAdapter(getActivity(), mCommentList);
            commentList.setAdapter(mCommentAdapter);
            detailCs.setVisibility(View.VISIBLE);
            addComment.setEnabled(true);
            tempSelected = mList.get(i).getUuid();
            pullAnnouncementDetail(mList.get(i).getUuid());
        }
    }

    //Firebase
    public void pullAnnouncements(){
        DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mList.add(new AnnouncementItem(dataSnapshot.child("title").getValue(String.class), dataSnapshot.child("date-posted").getValue(String.class), dataSnapshot.getKey()));

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mAdapter.notifyDataSetChanged();
            }
        });
    }
    public void pullAnnouncementDetail(String uuid){
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements").child(uuid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                title.setText(dataSnapshot.child("title").getValue(String.class));
                body.setText(dataSnapshot.child("body").getValue(String.class));
                author.setText(dataSnapshot.child("author").getValue(String.class));
                date.setText(dataSnapshot.child("date").getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final DatabaseReference commentRef = database.getReference().child("Companies")
                .child(mCompanyId).child("announcements").child(uuid).child("comments");
        commentRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mCommentList.add(new CommentItem(dataSnapshot.getKey(), dataSnapshot.child("name").getValue(String.class)
                        , dataSnapshot.child("comment").getValue(String.class)
                        , dataSnapshot.child("date-posted").getValue(String.class)));
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mAdapter.notifyDataSetChanged();
            }
        });

    }

}
