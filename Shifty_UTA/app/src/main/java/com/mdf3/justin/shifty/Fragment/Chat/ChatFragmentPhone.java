package com.mdf3.justin.shifty.Fragment.Chat;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mdf3.justin.shifty.R;

/**
 * Created by Torres on 11/3/17.
 */

public class ChatFragmentPhone extends Fragment {
    public static ChatFragmentPhone newInstance() {
        Bundle args = new Bundle();
        ChatFragmentPhone fragment = new ChatFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
