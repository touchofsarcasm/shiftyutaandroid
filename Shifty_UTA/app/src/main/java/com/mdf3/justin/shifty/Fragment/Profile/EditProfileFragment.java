package com.mdf3.justin.shifty.Fragment.Profile;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mdf3.justin.shifty.R;

import java.io.ByteArrayOutputStream;

/**
 * Created by Torres on 10/21/17.
 */

public class EditProfileFragment extends Fragment implements View.OnClickListener {

    //Firebase
    private static final String TAG = "EditProfileFragment";
    public static final String TAG_Main = "EditProfileFragmentTag";
    StorageReference mStorageRef;
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    ImageView editImage;
    TextView workId;
    TextView name;
    TextView accessLevel;
    EditText editPhone;
    EditText editEmail;
    Button changePassword;
    String mCompanyId;
    private static final int REQUEST_CAMERA_PERMISSION = 0x01001;
    private static final int REQUEST_IMAGE_CAPTURE = 0x01002;

    public static EditProfileFragment newInstance() {
        Bundle args = new Bundle();
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    //enabling access to appbar
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(true);
        menu.findItem(R.id.cancel_profile_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.cancel_profile_menu:
                cancelEditProfile();
                break;
            case R.id.save_profile_menu:
                saveEditProfile();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        editImage = (ImageView) getActivity().findViewById(R.id.edit_profile_image);
        editEmail = (EditText) getActivity().findViewById(R.id.edit_email);
        editPhone = (EditText) getActivity().findViewById(R.id.edit_phone_number);
        workId = (TextView) getActivity().findViewById(R.id.edit_work_id_text);
        name = (TextView) getActivity().findViewById(R.id.edit_name_text);
        accessLevel = (TextView) getActivity().findViewById(R.id.edit_access_level_text);
        changePassword = (Button) getActivity().findViewById(R.id.change_password_button);
        changePassword.setOnClickListener(this);
        editImage.setOnClickListener(this);
        pullFromFirebase();

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.change_password_button){
            //change password (Later)
            showChangePasswordDialog();
        } else if (view.getId() == R.id.edit_profile_image){
            openCamera();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            editImage.setImageBitmap(imageBitmap);
        }
    }
    private void openCamera(){
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //Device has a normal android camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }
    public void saveEditProfile(){
        EditProfileFragment fragment = (EditProfileFragment) getFragmentManager().findFragmentByTag("EditProfileFragmentTag");
        fragment.saveToFireBase();
        //StorageReference storageReference = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        StorageReference imageRef = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        //Getting image view as bytes
        editImage.setDrawingCacheEnabled(true);
        editImage.buildDrawingCache();
        Bitmap bmp = editImage.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        imageRef.putBytes(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
                ref.child("image").setValue(downloadUrl);
                Toast.makeText(getContext(), "Image saved", Toast.LENGTH_LONG).show();
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        ProfileFragment.newInstance()
                ).commit();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "Image failed to save", Toast.LENGTH_LONG).show();
            }
        });
    }
    public void cancelEditProfile(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ProfileFragment.newInstance()
        ).commit();
    }
    private void showChangePasswordDialog(){
        AlertDialog.Builder newPasswordDialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater customInflator = getActivity().getLayoutInflater();
        final View dialogView = customInflator.inflate(R.layout.edit_profile_change_password_dialog, null);
        newPasswordDialogBuilder.setView(dialogView);
        final EditText newPasswordET = (EditText) dialogView.findViewById(R.id.new_password_input);
        newPasswordDialogBuilder.setTitle("Change Password");
        newPasswordDialogBuilder.setMessage("Please Enter your old Password then your new one.(At least 6 Characters)");
        newPasswordDialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //change password in firebase
                if(newPasswordET.getText().toString().isEmpty()){
                    //one of the fields is empty
                    Toast.makeText(getContext(), "No Password Entered, Please try again.", Toast.LENGTH_LONG).show();
                } else if (newPasswordET.getText().toString().length() < 6) {
                    //password too short
                    Toast.makeText(getContext(), "New Password was not atleast 6 characters, Please try again.", Toast.LENGTH_LONG).show();
                } else {
                    //all good change password
                    FirebaseAuth.getInstance().getCurrentUser().updatePassword(newPasswordET.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getContext(), "Password Updated!", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //failed
                            Toast.makeText(getContext(), "Failed to change password in firebase", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        });
        newPasswordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //do nothing
            }
        });
        AlertDialog mainAlert = newPasswordDialogBuilder.create();
        mainAlert.show();
    }
    //Firebase Funcs
    public void pullFromFirebase(){
        final StorageReference imageRef = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        final long ONE_MEGABYTE = 1024 * 1024;
        imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                editImage.setImageBitmap(bmp);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "No Profile Picture found", Toast.LENGTH_SHORT).show();
            }
        });
        final String uuid = currentUser.getUid();
        final DatabaseReference ref = database.getReference("Users").child(uuid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mCompanyId = dataSnapshot.child("company-uuid").getValue(String.class);
                final DatabaseReference companyRef = database.getReference("Companies").child(mCompanyId).child("employees").child(uuid);
                companyRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        name.setText(dataSnapshot.child("name").getValue(String.class));
                        editEmail.setText(dataSnapshot.child("email").getValue(String.class));
                        accessLevel.setText(dataSnapshot.child("occupation").getValue(String.class));
                        if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                            editPhone.setText(dataSnapshot.child("phone-number").getValue(String.class));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //no update
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //no update
            }
        });

    }
    public void saveToFireBase(){
        if(editEmail.getText().toString().isEmpty() || editPhone.getText().toString().isEmpty()){

        } else {
            //updating email
            currentUser.updateEmail(editEmail.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "onSuccess: ");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "onFailure: ");
                }
            });
            //updating phone number (and image later)
            DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
            ref.child("email").setValue(editEmail.getText().toString());
            ref.child("phone-number").setValue(editPhone.getText().toString());
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    ProfileFragment.newInstance()
            ).commit();
        }
    }


}
