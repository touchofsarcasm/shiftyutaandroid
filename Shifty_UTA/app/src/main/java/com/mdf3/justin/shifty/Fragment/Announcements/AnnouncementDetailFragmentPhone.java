package com.mdf3.justin.shifty.Fragment.Announcements;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.CommentHelper.CommentAdapter;
import com.mdf3.justin.shifty.Helper.AnnouncementHelper.CommentHelper.CommentItem;
import com.mdf3.justin.shifty.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Torres on 10/29/17.
 */

public class AnnouncementDetailFragmentPhone extends Fragment {

    private static final String TAG = "AnnouncementDetailFragm";
    TextView title;
    TextView body;
    TextView author;
    TextView date;
    ListView commentList;
    String uuid;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    ArrayList<CommentItem> mList;
    CommentAdapter mAdapter;
    Calendar mCal;


    public static AnnouncementDetailFragmentPhone newInstance() {

        Bundle args = new Bundle();

        AnnouncementDetailFragmentPhone fragment = new AnnouncementDetailFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCal = Calendar.getInstance();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.add_comment_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.add_comment_menu){
            AlertDialog.Builder newPasswordDialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater customInflator = getActivity().getLayoutInflater();
            final View dialogView = customInflator.inflate(R.layout.edit_profile_change_password_dialog, null);
            newPasswordDialogBuilder.setView(dialogView);
            final EditText newPasswordET = (EditText) dialogView.findViewById(R.id.new_password_input);
            newPasswordET.setInputType(InputType.TYPE_CLASS_TEXT);
            newPasswordDialogBuilder.setTitle("Add Comment");
            newPasswordDialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(newPasswordET.getText().toString().isEmpty()){
                        Toast.makeText(getContext(), "Comment was empty", Toast.LENGTH_SHORT).show();
                    } else {
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Companies").child(mCompanyId)
                                .child("announcements").child(uuid).child("comments").child(UUID.randomUUID().toString());
                        ref.child("comment").setValue(newPasswordET.getText().toString());
                        ref.child("authorID").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
                        String date = dateFormat.format(mCal.getTime());
                        ref.child("date-posted").setValue(date);
                        Toast.makeText(getContext(), "Comment Posted", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            newPasswordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            AlertDialog mainAlert = newPasswordDialogBuilder.create();
            mainAlert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.announcement_detail_fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        title = (TextView) getActivity().findViewById(R.id.announcement_detail_title);
        body = (TextView) getActivity().findViewById(R.id.announcement_detail_body);
        author = (TextView) getActivity().findViewById(R.id.announcement_detail_author);
        date = (TextView) getActivity().findViewById(R.id.announcement_detail_date);
        commentList = (ListView) getActivity().findViewById(R.id.announcements_comment_list);
        uuid = getArguments().getString("announcementID");
        mCompanyId = getArguments().getString("announcementCompanyStringDetail");
        mList = new ArrayList<>();
        mAdapter = new CommentAdapter(getActivity(), mList);
        commentList.setAdapter(mAdapter);
        pullAnnouncementDetail();
    }

    public void pullAnnouncementDetail(){
        Log.d(TAG, "pullAnnouncementDetail: " + uuid + mCompanyId);
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("announcements").child(uuid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                title.setText(dataSnapshot.child("title").getValue(String.class));
                body.setText(dataSnapshot.child("body").getValue(String.class));
                author.setText(dataSnapshot.child("author").getValue(String.class));
                date.setText(dataSnapshot.child("date").getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        final DatabaseReference commentRef = database.getReference().child("Companies")
                .child(mCompanyId).child("announcements").child(uuid).child("comments");
        commentRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mList.add(new CommentItem(dataSnapshot.getKey(), dataSnapshot.child("name").getValue(String.class)
                        , dataSnapshot.child("comment").getValue(String.class)
                        , dataSnapshot.child("date-posted").getValue(String.class)));
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mAdapter.notifyDataSetChanged();
            }
        });



    }



}
