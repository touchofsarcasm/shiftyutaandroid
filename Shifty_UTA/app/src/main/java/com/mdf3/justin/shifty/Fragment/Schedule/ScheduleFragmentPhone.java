package com.mdf3.justin.shifty.Fragment.Schedule;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mdf3.justin.shifty.Helper.ScheduleHelper.ScheduleAdapterPhone;
import com.mdf3.justin.shifty.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Torres on 10/26/17.
 */

public class ScheduleFragmentPhone extends Fragment implements AdapterView.OnItemClickListener{
    private ListView scheduleList;
    private ArrayList<String> mList;
    private ScheduleAdapterPhone mAdapter;

    public static ScheduleFragmentPhone newInstance() {
        Bundle args = new Bundle();
        ScheduleFragmentPhone fragment = new ScheduleFragmentPhone();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.time_off_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.time_off_menu){
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    TimeOffRequestFragment.newInstance()
            ).addToBackStack(null).commit();
        }
        return super.onOptionsItemSelected(item);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_fragment_phone, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        int delta;
        mList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        Calendar now = Calendar.getInstance();
        delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2;
        now.add(Calendar.DAY_OF_MONTH, delta);
        for(int i = 0; i<7; i++){
            mList.add(format.format(now.getTime()));
            now.add(Calendar.DAY_OF_MONTH, 1);
        }
        scheduleList = (ListView) getActivity().findViewById(R.id.schedule_list_phone);
        mAdapter = new ScheduleAdapterPhone(getContext(), mList);
        scheduleList.setAdapter(mAdapter);
        scheduleList.setOnItemClickListener(this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String selectedDay = mList.get(i);
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ScheduleDetailFragmentPhone.newInstance(selectedDay)
        ).addToBackStack(null).commit();
    }
}
