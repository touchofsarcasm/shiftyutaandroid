package com.mdf3.justin.shifty.Helper.AnnouncementHelper.CommentHelper;

/**
 * Created by Torres on 10/29/17.
 */

public class CommentItem {
    String uuid;
    String name;
    String comment;
    String date;

    public CommentItem(String uuid, String name, String comment, String date) {
        this.uuid = uuid;
        this.name = name;
        this.comment = comment;
        this.date = date;
    }


    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public String getDate() {
        return date;
    }
}
