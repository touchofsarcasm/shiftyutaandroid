package com.mdf3.justin.shifty.Fragment.Employees;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.GridView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mdf3.justin.shifty.Helper.EmployeeHelper.EmployeeAdapter;
import com.mdf3.justin.shifty.Helper.EmployeeHelper.EmployeeItem;
import com.mdf3.justin.shifty.R;

import java.util.ArrayList;

/**
 * Created by Torres on 10/21/17.
 */

public class EmployeesFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = "EmployeesFragment";
    private GridView mGrid;
    private EmployeeAdapter mAdapter;
    private ArrayList<EmployeeItem> mList;
    private ArrayList<EmployeeItem> deleteArray;
    private Boolean mEdit = false;
    Boolean nullBool = null;
    private Menu mMenu;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();


    public static EmployeesFragment newInstance() {
        Bundle args = new Bundle();
        EmployeesFragment fragment = new EmployeesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.employees_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGrid = (GridView) getActivity().findViewById(R.id.employees_grid_view);
        mList = new ArrayList<>();
        deleteArray = new ArrayList<>();
        mAdapter = new EmployeeAdapter(getActivity(), mList, mEdit);
        mGrid.setAdapter(mAdapter);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        mGrid.setOnItemClickListener(this);
        pullEmployeesFromFirebase();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "onItemClick: " + mEdit);
        if(mEdit){
            //editing on
            Log.d(TAG, "onItemClick: ");
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.employee_on_edit_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);
                //mList.add(deleteArray.get(i));
                deleteArray.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteArray.add(mList.get(i));
                //mList.remove(deleteArray.get(i));
            }
        } else {
            Log.d(TAG, "onItemClick: ");
            //editing off
            Bundle detailBundle = new Bundle();
            DetailFragment fragment = DetailFragment.newInstance();
            detailBundle.putString("companyStringDetail", mCompanyId);
            detailBundle.putString("listId", mList.get(i).getUuid());
            fragment.setArguments(detailBundle);
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    fragment
            ).addToBackStack(null).commit();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
            menu.findItem(R.id.edit_employee_menu).setVisible(true);
            menu.findItem(R.id.add_employee_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: ");
        switch (item.getItemId()){
            case R.id.add_employee_menu:
                showAddEmployee();
                break;
            case R.id.edit_employee_menu:
                editEmployees();
                break;
            case R.id.save_edit_employee_menu:
                deleteSelectedEmployees();
                break;
            case R.id.cancel_edit_employee_menu:
                cancelEditEmployees();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void showAddEmployee(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                AddEmployee.newInstance()
        ).addToBackStack(null).commit();
    }
    public void cancelEditEmployees(){
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.add_employee_menu).setVisible(true);
    }
    public void editEmployees(){
        mEdit = true;
        mAdapter.updateEdit(true);
        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.add_employee_menu).setVisible(false);

    }
    public void deleteSelectedEmployees(){
        for(EmployeeItem employee: deleteArray){
            final DatabaseReference userToDelete = database.getReference()
                    .child("Companies").child(mCompanyId).child("employees").child(employee.getUuid());
            userToDelete.removeValue();
        }
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                EmployeesFragment.newInstance()
        ).commit();
    }
    //Firebase Func
    public void pullEmployeesFromFirebase(){
            Log.d(TAG, "pullEmployeesFromFirebase: " + mCompanyId);
            //company id was already found
            final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees");
            ref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mList.add(new EmployeeItem(dataSnapshot.child("name").getValue(String.class),
                            dataSnapshot.child("image").getValue(String.class), dataSnapshot.getKey(), dataSnapshot.child("occupation").getValue(String.class)));
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mAdapter.notifyDataSetChanged();
                }
            });
    }

}
