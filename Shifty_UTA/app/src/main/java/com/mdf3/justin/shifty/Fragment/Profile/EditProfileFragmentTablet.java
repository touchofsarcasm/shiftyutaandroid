package com.mdf3.justin.shifty.Fragment.Profile;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mdf3.justin.shifty.R;

import java.io.ByteArrayOutputStream;

/**
 * Created by Torres on 10/22/17.
 */

public class EditProfileFragmentTablet extends Fragment {

    public static final String TAG_Main = "EditProfileFragmentTagTablet";
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    String mCompanyId;
    EditText phone;
    EditText email;
    Button changeImage;
    Bitmap bitToSave;
    StorageReference mStorageRef;
    private static final int REQUEST_IMAGE_CAPTURE = 0x01002;

    public static Fragment newInstance() {
        Bundle args = new Bundle();
        EditProfileFragmentTablet fragment = new EditProfileFragmentTablet();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.save_profile_menu).setVisible(true);
        menu.findItem(R.id.cancel_profile_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_profile_menu:
                saveEditProfileTablet();
                break;
            case R.id.cancel_profile_menu:
                cancelEditProfileTablet();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_fragment_tablet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        email = (EditText) getActivity().findViewById(R.id.email_tablet_input);
        phone = (EditText) getActivity().findViewById(R.id.phone_tablet_input);
        changeImage = (Button) getActivity().findViewById(R.id.change_picture_button);
        pullFromFirebase();
        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            bitToSave = (Bitmap) extras.get("data");
        }
    }

    private void openCamera(){
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //Device has a normal android camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    public void saveEditProfileTablet(){
        EditProfileFragmentTablet tabletFragment = (EditProfileFragmentTablet) getFragmentManager().findFragmentByTag("EditProfileFragmentTagTablet");
        tabletFragment.saveToFirebaseTablet();
        StorageReference imageRef = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitToSave.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        imageRef.putBytes(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
                ref.child("image").setValue(downloadUrl);
                Toast.makeText(getContext(), "Image saved", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "Image failed to save", Toast.LENGTH_LONG).show();
            }
        });
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ProfileFragment.newInstance()
        ).commit();
    }
    public void cancelEditProfileTablet(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                ProfileFragment.newInstance()
        ).commit();
    }


    //Firebase Func
    public void pullFromFirebase(){
        final String uuid = currentUser.getUid();
        final DatabaseReference ref = database.getReference("Users").child(uuid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mCompanyId = dataSnapshot.child("company-uuid").getValue(String.class);
                final DatabaseReference companyRef = database.getReference("Companies").child(mCompanyId).child("employees").child(uuid);
                companyRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        email.setText(dataSnapshot.child("email").getValue(String.class));
                        if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                            phone.setText(dataSnapshot.child("phone-number").getValue(String.class));
                        } else {
                            phone.setText("Current Phone Number");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //no update
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //no update
            }
        });

    }
    public void saveToFirebaseTablet(){
        if(email.getText().toString().isEmpty() || phone.getText().toString().isEmpty()){

        } else {
            //updating email
            currentUser.updateEmail(email.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
            //updating phone number (and image later)
            DatabaseReference ref = database.getReference("Companies").child(mCompanyId).child("employees").child(currentUser.getUid());
            ref.child("email").setValue(email.getText().toString());
            ref.child("phone-number").setValue(phone.getText().toString());
            getFragmentManager().beginTransaction().replace(
                    R.id.core_activity_main_content,
                    ProfileFragment.newInstance()
            ).commit();
        }
    }


}
