package com.mdf3.justin.shifty.Helper.EmployeeHelper;

import android.support.annotation.Nullable;

/**
 * Created by Torres on 10/21/17.
 */

public class EmployeeItem {
    String name;
    String image;
    String uuid;
    String access;

    public EmployeeItem(String name, @Nullable String image, String uuid, String access) {
        this.name = name;
        this.image = image;
        this.uuid = uuid;
        this.access = access;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUuid() {
        return uuid;
    }

    public String getAccess() {
        return access;
    }

}
