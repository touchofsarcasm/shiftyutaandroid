package com.mdf3.justin.shifty.Fragment.Employees;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mdf3.justin.shifty.Helper.EmployeeHelper.EmployeeAdapterTablet;
import com.mdf3.justin.shifty.Helper.EmployeeHelper.EmployeeItem;
import com.mdf3.justin.shifty.R;

import java.util.ArrayList;

/**
 * Created by Torres on 10/21/17.
 */

public class EmployeesFragmentTablet extends Fragment implements AdapterView.OnItemClickListener {

    ImageView detailImage;
    TextView detailWorkId;
    TextView detailName;
    TextView detailAccess;
    TextView detailPhone;
    TextView detailEmail;
    Boolean mEdit = false;
    Menu mMenu;
    ConstraintLayout detailContainer;
    private ListView mListView;
    private StorageReference mStorageRef;
    private EmployeeAdapterTablet mAdapter;
    private ArrayList<EmployeeItem> mList;
    private ArrayList<EmployeeItem> deleteArray;
    String mCompanyId;
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static EmployeesFragmentTablet newInstance() {
        Bundle args = new Bundle();
        EmployeesFragmentTablet fragment = new EmployeesFragmentTablet();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        menu.findItem(R.id.edit_profile_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(true);
        menu.findItem(R.id.edit_employee_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_employee_menu:
                showAddProfileTablet();
                break;
            case R.id.edit_employee_menu:
                editEmployees();
                break;
            case R.id.save_edit_employee_menu:
                deleteSelectedEmployees();
                break;
            case R.id.cancel_edit_employee_menu:
                cancelEditEmployees();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.employee_fragment_tablet, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        deleteArray = new ArrayList<>();
        detailImage = (ImageView) getActivity().findViewById(R.id.employee_detail_image_tablet);
        detailWorkId = (TextView) getActivity().findViewById(R.id.employee_work_id_tablet);
        detailName = (TextView) getActivity().findViewById(R.id.employee_detail_name);
        detailAccess = (TextView) getActivity().findViewById(R.id.employee_detail_access);
        detailPhone = (TextView) getActivity().findViewById(R.id.employee_detail_phone);
        detailEmail = (TextView) getActivity().findViewById(R.id.employee_detail_email);
        detailContainer = (ConstraintLayout) getActivity().findViewById(R.id.detail_container);
        detailContainer.setVisibility(View.INVISIBLE);
        mListView = (ListView) getActivity().findViewById(R.id.employee_list_tablet);
        mList = new ArrayList<>();
        mAdapter = new EmployeeAdapterTablet(getContext(), mList, mEdit);
        mListView.setAdapter(mAdapter);
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        mListView.setOnItemClickListener(this);
        pullEmployeesFromFirebase();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(mEdit){
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.tablet_employee_checkbox);
            if(checkBox.isChecked()){
                checkBox.setChecked(false);
                deleteArray.remove(mList.get(i));

            } else {
                checkBox.setChecked(true);
                deleteArray.add(mList.get(i));
            }
        }
            detailContainer.setVisibility(View.VISIBLE);
            detailWorkId.setText("Work ID");
            detailName.setText("Name");
            detailPhone.setText("Phone Number");
            detailEmail.setText("Email");
            detailAccess.setText("Access Level");
            detailImage.setImageDrawable(getActivity().getDrawable(R.drawable.shifty_logo));
            pullEmployeeDetailFromFirebase(mList.get(i).getUuid());
    }

    public void showAddProfileTablet(){
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                AddEmployee.newInstance()
        ).addToBackStack(null).commit();
    }
    public void cancelEditEmployees(){
        mEdit = false;
        mAdapter.updateEdit(false);
        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.add_employee_menu).setVisible(true);
    }
    public void editEmployees(){
        mEdit = true;
        mAdapter.updateEdit(true);
        mMenu.findItem(R.id.save_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(true);
        mMenu.findItem(R.id.edit_employee_menu).setVisible(false);
        mMenu.findItem(R.id.add_employee_menu).setVisible(false);

    }
    public void deleteSelectedEmployees(){
        for(EmployeeItem employee: deleteArray){
            final DatabaseReference userToDelete = FirebaseDatabase.getInstance().getReference()
                    .child("Companies").child(mCompanyId).child("employees").child(employee.getUuid());
            userToDelete.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    mEdit = false;
                    mAdapter.updateEdit(false);
                    mMenu.findItem(R.id.save_edit_employee_menu).setVisible(false);
                    mMenu.findItem(R.id.cancel_edit_employee_menu).setVisible(false);
                    mMenu.findItem(R.id.edit_employee_menu).setVisible(true);
                    mMenu.findItem(R.id.add_employee_menu).setVisible(true);
                }
            });
        }
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                EmployeesFragmentTablet.newInstance()
        ).commit();
    }


    //Firebase Func
    public void pullEmployeesFromFirebase(){
        //company id was already found
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mList.add(new EmployeeItem(dataSnapshot.child("name").getValue(String.class),
                        dataSnapshot.child("image").getValue(String.class), dataSnapshot.getKey(), dataSnapshot.child("occupation").getValue(String.class)));
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mAdapter.notifyDataSetChanged();
            }
        });
    }
    public void pullEmployeeDetailFromFirebase(String UUID){
        final StorageReference imageRef = mStorageRef.child("profile_images/" + UUID);
        final long ONE_MEGABYTE = 1024 * 1024;
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees").child(UUID);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                detailName.setText(dataSnapshot.child("name").getValue(String.class));
                detailEmail.setText(dataSnapshot.child("email").getValue(String.class));
                detailAccess.setText(dataSnapshot.child("occupation").getValue(String.class));
                if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                    //phone exists
                    detailPhone.setText(dataSnapshot.child("phone-number").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //nothing
            }
        });
        imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                detailImage.setImageBitmap(bmp);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "No Profile Picture found", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
