package com.mdf3.justin.shifty.Activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mdf3.justin.shifty.Fragment.Announcements.AnnouncementFragmentPhone;
import com.mdf3.justin.shifty.Fragment.Announcements.AnnouncementFragmentTablet;
import com.mdf3.justin.shifty.Fragment.Chat.ChatFragmentPhone;
import com.mdf3.justin.shifty.Fragment.Chat.ChatFragmentTablet;
import com.mdf3.justin.shifty.Fragment.Employees.EmployeesFragment;
import com.mdf3.justin.shifty.Fragment.Employees.EmployeesFragmentTablet;
import com.mdf3.justin.shifty.Fragment.Profile.ProfileFragment;
import com.mdf3.justin.shifty.Fragment.Schedule.ScheduleFragment;
import com.mdf3.justin.shifty.Fragment.Schedule.ScheduleFragmentPhone;
import com.mdf3.justin.shifty.R;

public class CoreActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //global variable for the menu for easy changing
    private static final String TAG = "CoreActivity";
    boolean mTablet;
    FirebaseAuth mAuth;
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.core_activity);
        //assigning toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        mAuth = FirebaseAuth.getInstance();
        mTablet = checkForDevice();
        companyIdPull();
        //starting on profile page
        getFragmentManager().beginTransaction().replace(R.id.core_activity_main_content,
                ProfileFragment.newInstance()).commit();
        //setup Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        //setup Nav/Drawer listener
        NavigationView navigationView = (NavigationView) findViewById(R.id.core_activity_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Fragment selectedFragment = null;
        Class selectedFragmentClass = null;
        switch (id){
            case R.id.core_activity_drawer_list_profile:
                selectedFragmentClass = ProfileFragment.class;
                break;
            case R.id.core_activity_drawer_list_employees:
                if(getResources().getBoolean(R.bool.isTablet)){
                    selectedFragmentClass = EmployeesFragmentTablet.class;
                } else {
                    selectedFragmentClass = EmployeesFragment.class;
                }
                break;
            case R.id.core_activity_drawer_list_schedule:
                if(getResources().getBoolean(R.bool.isTablet)){
                    selectedFragmentClass = ScheduleFragment.class;
                } else {
                    selectedFragmentClass = ScheduleFragmentPhone.class;
                }
                break;
            case R.id.core_activity_drawer_list_announcements:
                if(getResources().getBoolean(R.bool.isTablet)){
                    selectedFragmentClass = AnnouncementFragmentTablet.class;
                } else {
                    selectedFragmentClass = AnnouncementFragmentPhone.class;
                }
                break;
            case R.id.core_activity_drawer_list_chat:
                if(getResources().getBoolean(R.bool.isTablet)) {
                    selectedFragmentClass = ChatFragmentTablet.class;
                } else {
                    selectedFragmentClass = ChatFragmentPhone.class;
                }
                break;
            case R.id.core_activity_drawer_list_logout:
                logoutOfFirebase();
                return true;
        }
        try{
            selectedFragment = (Fragment) selectedFragmentClass.newInstance();
        } catch (Exception e){
            e.printStackTrace();
        }
        getFragmentManager().beginTransaction().replace(R.id.core_activity_main_content,
                selectedFragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public Boolean checkForDevice(){
        //boolean determining tablet or not
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        if (isTablet) {
            //Current Device is Tablet
            //setting orientation to landscape
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            //Current Device is Phone
            //setting orientation to phone
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        return isTablet;
    }
    //Firebase Func
    public void companyIdPull(){
        final String uuid = currentUser.getUid();
        final DatabaseReference ref = database.getReference("Users").child(uuid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                SharedPreferences pref =  getSharedPreferences("pref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(getString(R.string.user_company_key), dataSnapshot.child("company-uuid").getValue(String.class));
                editor.putString(getString(R.string.user_access_level), dataSnapshot.child("occupation").getValue(String.class));
                editor.apply();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //nothing
            }
        });
    }
    public void logoutOfFirebase(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        finish();
        Intent startIntent = new Intent();
        startIntent.setClass(getApplicationContext(), StartActivity.class);
        startIntent.setFlags(startIntent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(startIntent);
        mAuth.signOut();
    }
}
