package com.mdf3.justin.shifty.Fragment.Profile;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mdf3.justin.shifty.R;

/**
 * Created by Torres on 10/20/17.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ProfileFragment";
    //universal
    ImageView profileImage;
    TextView workId;
    TextView name;
    TextView accessLevel;
    TextView phoneNumber;
    TextView emailT;
    String mCompanyId;
    String mAccessLevel;
    private StorageReference mStorageRef;
    //tablet
    Button editProfile;
    Button changePassword;
    //Firebase
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    //enabling access to appbar
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(getResources().getBoolean(R.bool.isTablet)){
            menu.findItem(R.id.edit_profile_menu).setVisible(false);
        } else {
            menu.findItem(R.id.edit_profile_menu).setVisible(true);
        }
        menu.findItem(R.id.edit_employee_menu).setVisible(false);
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        menu.findItem(R.id.save_profile_menu).setVisible(false);
        menu.findItem(R.id.cancel_profile_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.edit_profile_menu){
        showEditProfile();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment, container, false);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        //Universal initialization
        SharedPreferences pref = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mCompanyId = pref.getString(getString(R.string.user_company_key), "N/A");
        boolean isTablet = getActivity().getResources().getBoolean(R.bool.isTablet);
        profileImage = (ImageView) getActivity().findViewById(R.id.profile_image);
        workId = (TextView) getActivity().findViewById(R.id.edit_work_id_text);
        name = (TextView) getActivity().findViewById(R.id.edit_name_text);
        accessLevel = (TextView) getActivity().findViewById(R.id.edit_access_level_text);
        phoneNumber = (TextView) getActivity().findViewById(R.id.phone_number_text);
        emailT = (TextView) getActivity().findViewById(R.id.email_text);
        if(isTablet){
            loadTabletUI();
        }
        pullFromFirebase();
        super.onActivityCreated(savedInstanceState);
    }

    //Tablet Functions
    public void loadTabletUI(){
        editProfile = (Button) getActivity().findViewById(R.id.edit_profile_button);
        changePassword = (Button) getActivity().findViewById(R.id.change_password_button);
        if(editProfile != null && changePassword != null){
            editProfile.setOnClickListener(this);
            changePassword.setOnClickListener(this);
        }

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_profile_button:
                //Load detail edit profile
                showTabletEditProfileFragment();
                break;
            case R.id.change_password_button:
                //Load detail change password
                showChangePasswordDialog();
                break;
        }
    }

    public void showEditProfile(){
        //phone will load edit profile screen
        getFragmentManager().beginTransaction().replace(
                R.id.core_activity_main_content,
                EditProfileFragment.newInstance(),
                EditProfileFragment.TAG_Main
        ).addToBackStack(null).commit();
    }
    private void showTabletEditProfileFragment(){
        //Replaces detail with edit profile fragment
        getFragmentManager().beginTransaction().replace(
                R.id.profile_container_right,
                EditProfileFragmentTablet.newInstance(),
                EditProfileFragmentTablet.TAG_Main
        ).commit();
    }
    private void showChangePasswordDialog(){
        AlertDialog.Builder newPasswordDialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater customInflator = getActivity().getLayoutInflater();
        final View dialogView = customInflator.inflate(R.layout.edit_profile_change_password_dialog, null);
        newPasswordDialogBuilder.setView(dialogView);
        final EditText newPasswordET = (EditText) dialogView.findViewById(R.id.new_password_input);
        newPasswordDialogBuilder.setTitle("Change Password");
        newPasswordDialogBuilder.setMessage("Please Enter your old Password then your new one.(At least 6 Characters)");
        newPasswordDialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //change password in firebase
                if(newPasswordET.getText().toString().isEmpty()){
                    //one of the fields is empty
                    Toast.makeText(getContext(), "No Password Entered, Please try again.", Toast.LENGTH_LONG).show();
                } else if (newPasswordET.getText().toString().length() < 6) {
                    //password too short
                    Toast.makeText(getContext(), "New Password was not atleast 6 characters, Please try again.", Toast.LENGTH_LONG).show();
                } else {
                            //all good change password
                            FirebaseAuth.getInstance().getCurrentUser().updatePassword(newPasswordET.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            SharedPreferences pref = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(getString(R.string.user_password_key), newPasswordET.getText().toString());
                            editor.apply();
                            Toast.makeText(getContext(), "Password Updated!", Toast.LENGTH_SHORT).show();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //failed
                            Toast.makeText(getContext(), "Failed to change password in firebase", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        });
        newPasswordDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //do nothing
            }
        });
        AlertDialog mainAlert = newPasswordDialogBuilder.create();
        mainAlert.show();
    }
    //Firebase Funcs
    public void pullFromFirebase(){
        final StorageReference imageRef = mStorageRef.child("profile_images/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        final long ONE_MEGABYTE = 1024 * 1024;
        final String uuid = currentUser.getUid();
        final DatabaseReference ref = database.getReference("Users").child(uuid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mCompanyId = dataSnapshot.child("company-uuid").getValue(String.class);
                final DatabaseReference companyRef = database.getReference("Companies").child(mCompanyId).child("employees").child(uuid);
                companyRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        name.setText(dataSnapshot.child("name").getValue(String.class));
                        emailT.setText(dataSnapshot.child("email").getValue(String.class));
                        accessLevel.setText(dataSnapshot.child("occupation").getValue(String.class));
                        if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                            //phone exists
                            phoneNumber.setText(dataSnapshot.child("phone-number").getValue(String.class));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //no update
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //no update
            }
        });
        imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                profileImage.setImageBitmap(bmp);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(getContext() != null){
                    Toast.makeText(getContext(), "No Profile Picture found", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
