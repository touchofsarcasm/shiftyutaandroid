package com.mdf3.justin.shifty.Fragment.Employees;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mdf3.justin.shifty.R;

/**
 * Created by Torres on 10/21/17.
 */

public class DetailFragment extends Fragment {
    ImageView image;
    TextView workId;
    TextView name;
    TextView accessLevel;
    TextView email;
    TextView phoneNumber;
    String mCompanyId;
    String mUUID;
    private StorageReference mStorageRef;
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static DetailFragment newInstance() {
        Bundle args = new Bundle();
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.employee_detail_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        image = (ImageView) getActivity().findViewById(R.id.employee_image);
        workId = (TextView) getActivity().findViewById(R.id.employee_work_id);
        name = (TextView) getActivity().findViewById(R.id.employee_name);
        accessLevel = (TextView) getActivity().findViewById(R.id.employee_access_level);
        email = (TextView) getActivity().findViewById(R.id.employee_email);
        phoneNumber = (TextView) getActivity().findViewById(R.id.employee_phone_number);
        mUUID = getArguments().getString("listId");
        mCompanyId = getArguments().getString("companyStringDetail");
        pullEmployeeDetailFromFirebase();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.add_employee_menu).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //Firebase
    public void pullEmployeeDetailFromFirebase(){
        final StorageReference imageRef = mStorageRef.child("profile_images/" + mUUID);
        final long ONE_MEGABYTE = 1024 * 1024;
        final DatabaseReference ref = database.getReference().child("Companies").child(mCompanyId).child("employees").child(mUUID);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //update ui
                name.setText(dataSnapshot.child("name").getValue(String.class));
                email.setText(dataSnapshot.child("email").getValue(String.class));
                accessLevel.setText(dataSnapshot.child("occupation").getValue(String.class));
                if(dataSnapshot.child("phone-number").getValue(String.class) != null){
                    //phone exists
                    phoneNumber.setText(dataSnapshot.child("phone-number").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //do nothing
            }
        });
        imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                image.setImageBitmap(bmp);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "No Profile Picture found", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
