package com.mdf3.justin.shifty.Helper.ScheduleHelper;

import android.support.annotation.Nullable;

/**
 * Created by Torres on 10/24/17.
 */

public class SelectedDayItem {
    String name;
    String shift;
    String image;
    String dateID;

    public SelectedDayItem(String name, String shift, @Nullable String image, String dateID) {
        this.name = name;
        this.shift = shift;
        this.image = image;
        this.dateID = dateID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDateID() {
        return dateID;
    }
}
