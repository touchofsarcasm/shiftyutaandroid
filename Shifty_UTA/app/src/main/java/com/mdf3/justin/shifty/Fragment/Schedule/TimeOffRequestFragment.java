package com.mdf3.justin.shifty.Fragment.Schedule;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mdf3.justin.shifty.R;

/**
 * Created by Torres on 10/26/17.
 */

public class TimeOffRequestFragment extends Fragment {
    public static TimeOffRequestFragment newInstance() {
        Bundle args = new Bundle();
        TimeOffRequestFragment fragment = new TimeOffRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.send_time_off_menu).setVisible(true);
        menu.findItem(R.id.cancel_time_off_menu).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.cancel_time_off_menu){
            if(getResources().getBoolean(R.bool.isTablet)){
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        ScheduleFragment.newInstance()
                ).commit();
            } else {
                getFragmentManager().beginTransaction().replace(
                        R.id.core_activity_main_content,
                        ScheduleFragmentPhone.newInstance()
                ).commit();
            }
        } else if (item.getItemId() == R.id.send_time_off_menu) {
            Toast.makeText(getContext(), "In time my child", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.time_off_request_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
